const test = require('tape');
const Rx = require('rx/dist/rx.lite');
require('./queue.js');

test('should execute process in right order', t => {
  const record = [];
  const result = [];

  const createObservableSequence = id => {
    return Rx.Observable.of({})
      .do(() => record.push({ id, message: 'step 1' }))
      .delay(1000)
      .do(() => record.push({ id, message: 'step 2' }))
      .delay(1000)
      .do(() => record.push({ id, message: 'step 3' }))
      .map(() => `finished${ id }`);
  };

  Rx.Observable
    .interval(1000)
    .take(3)
    .queue(id => createObservableSequence(id))
    .subscribe({
      onNext: output => result.push(output),
      onError: error => t.fail(error),
      onCompleted: () => {
        t.deepEqual(result, [ [ 'finished0' ], [ 'finished1' ], [ 'finished2' ] ]);
        t.deepEqual(record, [
          { id: 0, message: 'step 1' },
          { id: 0, message: 'step 2' },
          { id: 0, message: 'step 3' },
          { id: 1, message: 'step 1' },
          { id: 1, message: 'step 2' },
          { id: 1, message: 'step 3' },
          { id: 2, message: 'step 1' },
          { id: 2, message: 'step 2' },
          { id: 2, message: 'step 3' },
        ]);
        t.end();
      }
    });
});
