const Rx = require('rx/dist/rx.lite');

Rx.Observable.prototype.queue = function (createObservable) {
  const queue = [];
  const source = this;
  let flushing = false;
  let theEnd = false;

  return Rx.Observable.create(observer => {
    const consume = () => {
      flushing = true;

      const payload = queue.pop();
      if (!payload) {
        flushing = false;
        if (theEnd) observer.onCompleted();
        return;
      }

      const asyncResults = [];
      payload.process.subscribe({
        onError: observer.onError.bind(observer),
        onNext: result => {
          asyncResults.push(result);
        },
        onCompleted: () => {
          observer.onNext(asyncResults);
          consume();
        }
      });
    };

    return source.subscribe(
      data => {
        queue.unshift({ process: createObservable(data) });
        if (!flushing) consume();
      },
      observer.onError.bind(observer),
      () => {
        theEnd = true;
        if (!flushing) consume();
      }
    );
  });
};
