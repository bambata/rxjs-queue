# RxJS Queue Operator

## Description

The `queue` operator is meant to serialize and pipeline the execution of the returned observable : this means that the observable generated
by the event of the source must complete before executing the next one and that behavior is repeated until the source completes.

Interesting use case is when you need to serialize execution of some steps within an asynchronous process because you are sharing some
resource. e.g. when using a stateful scene object within an observable to generate images with that scene object, we wants to avoid race condition
where the image generating correspond to the next event scene setup.

```
// Load Rx (version 4)
// requiring the lib will add the operator
// to the Observable's prototype

const Rx = require('rx/dist/rx.lite');
require('rxjs-queue');

// Dummy example to show syntax
// where the pipeline process
// does nothing special

sourceObservable.queue((event) => {
  return Observable.of(event)
});
```

The project contains a unit test with a more advanced utilization.

## Caveats

The operator is targeting RxJS 4 just because that is were I need it, could be easily adapted to RxJS 5.
